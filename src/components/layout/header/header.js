import React from 'react'
import { useHistory } from 'react-router-dom'
import { removeToken, isAuthenticated } from '../../../config/auth'
import './header.css'
import { Button } from 'reactstrap';
import imgLogo from '../../../assets/images/logo_organic.png'


const Header = (props) => {
    const history = useHistory()

    const logout = () => {
        removeToken()
        history.push('/login')
    }

    const hasUser = () => {
        if (props.info && props.info.email) {
            return (                
                <>
                    <i className="fa fa-user" /> {props.info.email} |
                </>
            )
        }
    }

    return (
        <header className="d-flex">
            <div className="copyRight">
            <img src={imgLogo} alt="Logo Organic Food" />
        </div>
            <div className="title">{props.title}</div>
            <div className="profile">
                {hasUser() }
                {isAuthenticated() ? (
                    <Button size="sm"  color="danger" className="logout" onClick={logout}> <i className="fa fa-sign-out"></i> Sair</Button>
                ) : ""}
            </div>
        </header>
    )
}

        


export default Header