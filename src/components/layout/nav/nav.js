import React from 'react'
import './nav.css'
import history from '../../../config/history'
import { Button } from 'reactstrap';

const Nav = ({ name, title, to }) => {

    const changePage = () => history.push(to)

    return (
        <nav className="d-flex">
            <div className="title"> 
                <h3>{title}</h3>
            </div>
            
            <div className="action">
                <Button color="success" onClick={changePage}>
                    {name}
                </Button>
            </div>
        </nav>
    )
}
export default Nav;