import React, { useState, useEffect } from 'react'
import { createUser, showUserId, updateUser } from '../../services/user'
// import Loading from '../loading/loading'
import Alert from '../alert/alert'
import './user.css'
import Nav from '../layout/nav/nav'
import { useHistory, useParams } from 'react-router-dom'
import jwt from 'jsonwebtoken'
import { getToken } from '../../config/auth'
import { Button, FormGroup, CustomInput } from 'reactstrap';


const UserCreate = (props) => {
    const [userIsAdmin, setUserIsAdmin] = useState({})
    const [isSubmit, setIsSubmit] = useState(false)
    const [isEdit, setisEdit] = useState(false)
    const [alert, setAlert] = useState({})
    const history = useHistory()
    const { id } = useParams()
    const methodUser = isEdit ? updateUser : createUser

    const [form, setForm] = useState({
        is_admin: false
    })

    useEffect(() => {
        (async () => {
            const { user } = await jwt.decode(getToken())
            setUserIsAdmin(user.is_admin)
        })()
        return () => { }
    }, [])

    useEffect(() => {
        const getShowUser = async () => {
            const user = await showUserId(id)
            if (user.data.senha) {
                delete user.data.senha
            }
            setForm(user.data)
        }


        if (id) {
            setisEdit(true)
            getShowUser()
        }

    }, [id])



    const handleChange = (event) => {
        

        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        const name = event.target.name;

        setForm({
            ...form,
            [name]: value
        });
    }

    const formIsValid = () => {
        return form.social && form.cnpj && form.endereco && form.cep && form.cidade && form.uf && form.telefone && form.email && form.senha
    }

    const submitForm = async (event) => {

        try {
            setIsSubmit(true)
            await methodUser(form)
            const is_admin = userIsAdmin ? form.is_admin : false
            setForm({
                ...form,
                is_admin
            })

            setAlert({
                type: "success",
                message: 'Seu formulário foi enviado com sucesso',
                show: true
            })
            setIsSubmit(false)

            setTimeout(() =>
                history.push('/')
                , 3000)
        } catch (e) {
            setAlert({
                type: "error",
                message: 'Ocorreu um erro no cadastro',
                show: true
            })
            setIsSubmit(false)
        }
    }

    return (

        <React.Fragment>
            <Nav name="Lista" title="Cadastro de usuários" to="/" />
            <section  className="fundo">
                <Alert type={alert.type || ""} message={alert.message || ""} show={alert.show || false} />

                <form className="create_user">
                    <div className="form_login">
                        <fieldset className="input-block">
                            <legend>Sua empresa</legend>

                            <FormGroup>
                                <input disabled={isSubmit} type="text" name="social" id="social" onChange={handleChange} value={form.social || ""} placeholder="Razão Social" />
                            </FormGroup>

                            <FormGroup>
                                <input disabled={isSubmit} type="text" name="cnpj" id="cnpj" onChange={handleChange} value={form.cnpj || ""} placeholder="CNPJ" />
                            </FormGroup>

                        </fieldset>

                        <fieldset className="input-block">

                            <legend>Onde você está?</legend>

                            <FormGroup>
                                <input disabled={isSubmit} type="text" name="endereco" id="endereco" onChange={handleChange} value={form.endereco || ""} placeholder="Endereço" />
                            </FormGroup>

                            <FormGroup>
                                <input disabled={isSubmit} type="text" name="cep" id="cep" min="8" max="8" onChange={handleChange} value={form.cep || ""} placeholder="CEP" />
                            </FormGroup>

                            <FormGroup>
                                <input disabled={isSubmit} type="text" name="cidade" id="cidade" onChange={handleChange} value={form.cidade || ""} placeholder="Cidade" />
                            </FormGroup>

                            <FormGroup>
                                <label htmlFor="uf">UF:&nbsp; </label>
                                <select disabled={isSubmit} name="uf" id="uf" onChange={handleChange} value={form.uf || ""} placeholder="UF">
                                    <option value="">Selecione</option>
                                    <option value="AC">AC</option>
                                    <option value="AL">AL</option>
                                    <option value="AP">AP</option>
                                    <option value="AM">AM</option>
                                    <option value="BA">BA</option>
                                    <option value="CE">CE</option>
                                    <option value="DF">DF</option>
                                    <option value="ES">ES</option>
                                    <option value="GO">GO</option>
                                    <option value="MA">MA</option>
                                    <option value="MS">MS</option>
                                    <option value="MT">MT</option>
                                    <option value="MG">MG</option>
                                    <option value="PA">PA</option>
                                    <option value="PB">PB</option>
                                    <option value="PR">PR</option>
                                    <option value="PE">PE</option>
                                    <option value="PI">PI</option>
                                    <option value="RJ">RJ</option>
                                    <option value="RN">RN</option>
                                    <option value="RS">RS</option>
                                    <option value="RO">RO</option>
                                    <option value="RR">RR</option>
                                    <option value="SC">SC</option>
                                    <option value="SP">SP</option>
                                    <option value="SE">SE</option>
                                    <option value="TO">TO</option>
                                </select>
                            </FormGroup>

                        </fieldset>

                        <fieldset className="input-block">
                            <legend>Detalhes de contato</legend>

                            <FormGroup>
                            <input disabled={isSubmit} type="tel" name="telefone" id="telefone" min="11" max="11" onChange={handleChange} value={form.telefone || ""} placeholder="Telefone" />
                            </FormGroup>
                            
                            <FormGroup>
                            <input disabled={isSubmit || isEdit} type="email" id="auth_email" name="email" onChange={handleChange} value={form.email || ""} placeholder="Email" />
                            </FormGroup>

                            <FormGroup>
                            <input disabled={isSubmit} type="password"
                                id="auth_password" name="senha"
                                onChange={handleChange}
                                value={form.senha || ""}
                                placeholder={isEdit ? `Atualize sua senha ` : 'Informe sua senha'} />
                            </FormGroup>


                        </fieldset>

                        <FormGroup>
                            {userIsAdmin ? (
                            <CustomInput type="switch" label="Ativar Usuário como administrador:" name="is_admin" id="is_admin" onChange={handleChange} checked={(!isEdit ? true : form.is_admin) || false} />
                            ) : ""}
                            <CustomInput type="switch" label="Usuário ativo ?" id="is_active" name="is_active" onChange={handleChange} checked={(!isEdit ? true : form.is_active) || false} />
                        </FormGroup>

                        <Button color="success" disabled={!formIsValid()} onClick={submitForm}>
                            {isEdit ? "Atualizar" : "Cadastrar"}
                        </Button>
                    </div>
                    <br />
                    {isSubmit ? <div>Carregando....</div> : ""}
                </form>
            </section >
        </React.Fragment>

    )
}

export default UserCreate

