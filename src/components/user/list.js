import React, { useEffect, useState } from 'react'
import { ListUser, DeleteUser } from '../../services/user'
import Loading from '../loading/loading'
import Nav from '../../components/layout/nav/nav'
import { Table, Button, Modal, ModalBody, ModalFooter } from 'reactstrap';

const UserList = (props) => {

    const [users, setUsers] = useState([])
    const [loading, setloading] = useState(false)
    const [isUpdate, setIsUpdate] = useState(false)
    const [confirmation, setConfirmation] = useState({
        isShow: false,
        params: {}
    })

    const editUser = (user) => props.history.push(`/edit/${user._id}`)

    const deleteUser = async () => {

        if (confirmation.params) {
            await DeleteUser(confirmation.params._id)
        }
        setConfirmation({
            isShow: false,
            params: {}
        })
        setIsUpdate(true)
    }

    const Confirmation = () => {
        const toggle = () => setConfirmation(!confirmation.isShow);
        return (
            <Modal isOpen={confirmation.isShow} toggle={toggle} className="info">
                <ModalBody>
                    Você deseja excluir a empresa {(confirmation.params && confirmation.params.social) || ""}
                </ModalBody>
                <ModalFooter>
                    <Button color="success" onClick={deleteUser}>SIM</Button>{' '}
                    <Button color="danger" onClick={toggle}>NÃO</Button>
                </ModalFooter>
            </Modal>
        )
    }

    const verifyIsEmpty = users.length === 0

    const sortList = (users) => {
        return users.sort((a, b) => {
            if (a.is_active < b.is_active) {
                return 1;
            }
            if (a.is_active > b.is_active) {
                return -1;
            }
            return 0;
        })
    }

    const setIcon = (conditional) => (
        <i className={`action fa fa-${conditional ? "check text-success" : "times text-danger"}`} />
    )

    const montarTabela = () => {
        const listSorted = sortList(users)

        const linhas = listSorted.map((user, index) => (
            <tr key={index} className={user.is_active ? "" : "table-danger"} >
                <td className="iconCenter"> {setIcon(user.is_active)}</td>
                <td className="iconCenter"> {setIcon(user.is_admin)}</td>
                <td>{user.social}</td>
                <td>{user.cnpj}</td>
                <td>{user.endereco}</td>
                <td>{user.cep}</td>
                <td>{user.cidade}</td>
                <td>{user.uf}</td>
                <td>{user.telefone}</td>
                <td>{user.email}</td>
                <td>
                    <span onClick={() => editUser(user)} className="text-success mx-1" >
                        <i className="action fa fa-edit"></i>
                    </span>
                    <span onClick={() => setConfirmation({ isShow: true, params: user })} className="text-danger  mx-1">
                        <i className="action fa fa-trash"></i>
                    </span>
                </td>
            </tr>
        ))

        return !verifyIsEmpty ? (
            <Table className="table table-striped table-sm">
                <thead>
                    <tr>                    
                        <th>ATIVO</th>
                        <th>ADMIN</th>
                        <th>RAZÃO SOCIAL</th>
                        <th>CNPJ</th>
                        <th>ENDEREÇO</th>
                        <th>CEP</th>
                        <th>CIDADE</th>
                        <th>UF</th>
                        <th>TELEFONE</th>
                        <th>EMAIL</th>
                        <th>AÇÕES</th>
                    </tr>                   
                </thead>
                
                <tbody>
                    {linhas}
                </tbody>
            </Table>
        ) : ""
    }

    const fncUseEffect = () => {
        let isCancelled = false;
        (async () => {
            try {
                setloading(true)
                const usersAll = await ListUser()
                if (!isCancelled) {
                    if (usersAll) {
                        setUsers(usersAll.data)
                    }
                    setloading(false)
                    setIsUpdate(false)
                }
            } catch (error) {
                setloading(false)
            }
        })()

        return () => { isCancelled = true }
    }

    // INIT 
    useEffect(fncUseEffect, [])

    // UPDATE 
    useEffect(fncUseEffect, [isUpdate])


    //render
    return (
        <div>
            <Nav name="Novo" title="Lista de usuários" to="/create" />
            <Confirmation />
            <section>
                <div className="list_user">
                    <Loading show={loading} />
                    {montarTabela()}
                </div>
            </section>
        </div>
    )
}

export default UserList