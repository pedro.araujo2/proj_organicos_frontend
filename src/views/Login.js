import React from 'react'
import LoginComponent from '../components/login/login'
import Layout from '../components/layout'

const Login = () => {
    return (
        <Layout>
            <LoginComponent />
        </Layout>
    )
}

export default Login
